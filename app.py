import configparser
import random

import aspace


def main():

    #
    # Migration Setup
    #

    config = configparser.ConfigParser()
    config.read([
        'settings.ini',
        'settings.local.ini'
    ])

    client = (
        aspace.client.ASpaceClient
        .init_from_config(config)
        .wait_until_ready(
            check_interval=2,
            max_wait_time=200.0,
            authenticate_on_success=True,
            on_fail=lambda: print("Waiting for ArchivesSpace... (Host: %s)" % config.get('aspace_credentials', 'api_host'))
        )
    )

    rng = random.Random()
    password_characters = config.get('new_password_settings', 'password_characters')
    password_length = int(config.get('new_password_settings', 'password_length'))

    def new_password_function(user: dict):
        username = user['username'].strip()

        new_pass = ''.join(rng.sample(
            password_characters,
            password_length
        ))
        
        print(username, '->', new_pass)
        return new_pass

    client.manage_users().change_all_passwords(
        new_password_function,
        include_admin=False
    )

    new_admin_pass = config.get('new_password_settings', 'new_admin_password')
    if new_admin_pass:
        result = client.manage_users().change_password(
            'admin',
            new_admin_pass)
            
        assert result.ok
        print('admin ->', new_admin_pass)

    print("Done.")


if __name__ == '__main__':
    main()

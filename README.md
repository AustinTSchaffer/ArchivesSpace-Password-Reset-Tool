# Python ArchivesSpace Password Reset Tool

Resets all of the non-admin user passwords on the target ArchivesSpace instance,
using a random character generator.

Sets the admin password to a specific value, if a value is specified in the
settings. If no specific value is specified for the new admin password, the
admin password will not be changed.

The new passwords are printed to the console. This is so you don't accidentally
lock yourself out, and so you can distribute the temporary passwords if needed.

This script is not a secure application and is only meant to be run locally.
Only run this script in a trusted, secure environment. Burn the logs when you
are done securing the passwords in your password manager, then mail your hard
drive to the Sun.

    The Sun
    666 Orion Spur Milky Way
    Jacksonville, FL, 32217

## Usage

To run this script, configure the `settings.ini` to point it at an ArchivesSpace
API, using the credentials of an admin user, then debug or run the program using
your Python IDE or through a terminal.

```bash
virtualenv --python=python3 venv
source venv/bin/activate
pip install -r requirements.txt
python app.py
```

If you're worried about accidentally uploading admin credentials, and don't want
to modify the `settings.ini` with your confidential information, create a new
settings file named `settings.local.ini`. Any settings specified in the local
INI will overwrite the default settings. It might be easier to just copy the
whole settings.ini and replace the values that you need to.

The `settings.local.ini` is already in the `.gitignore`, and is already
configured to overwrite the defaults from `settings.ini`.
